<?php
	class storage
	{
		function __construct()
		{
			$this->db = new db(DB_DSN, DB_USER, DB_PASSWORD);
		}

		public function addProfile( $userAgent, $cookies, $screen, $age, $gender, $country, $region, $city, $areaName )
		{
			$this->db->insert( 'profiles', array(
								'useragent' => $userAgent, 'cookies' => json_encode($cookies), 'screen' => json_encode($screen),
								'age' => (int)$age, 'gender' => (int)$gender, 'country' => $country, 'region' => $region,
								'city' => $city, 'area' => $areaName
						)
			);
			return $this->db->lastInsertedId();
		}

		public function getProfile( $ID )
		{
			if($profile = $this->db->select('profiles', 'ID = :id', array(':id' => $ID)));
				return $profile[0];
			return false;
		}

		public function getInfoArea( $areaName )
		{
			$area = [];
			$result = $this->db->select('profiles', 'area = :areaname', array(':areaname' => $areaName));
			if(!$result) return $area;

			foreach($result as $p)
			{
				$profile = array();
				foreach(array('ID', 'country', 'gender', 'region', 'age', 'city') as $key)
					$profile[$key] = $p[$key];
				$area[] = $profile;
			}

			return $area;
		}

		public function updateProfile( $id, $cookies, $screen )
		{
			$this->db->update( 'profiles', array('cookies' => json_encode($cookies), 'screen' => json_encode($screen)), 'ID = :id', array(':id' => $id));
		}

		public function removeProfile( $id )
		{
			return $this->db->delete('profiles', 'ID = :id', array(':id' => $id));
		}

		public function addItem( $name, $value, $namespace = 'public' )
		{
			if($item = $this->getItem($name, $namespace))
			{
				$this->updateItem($item['ID'], $value);
				return $item['ID'];
			}

			$this->db->insert('items', array('name' => $name, 'value' => $value, 'namespace' => $namespace));
			return $this->db->lastInsertedId();
		}

		public function removeItem( $name, $namespace = 'public' )
		{
			return $this->db->delete('items', 'name = :name AND namespace = :namespace', array(':name' => $name, ':namespace' => $namespace));
		}

		public function updateItem( $id, $value )
		{
			return $this->db->update('items', array('value' => $value), 'ID = :id', array(':id' => $id));
		}

		public function removeItemById( $id )
		{
			return $this->db->delete('items', 'ID = :id', array(':id' => $id));
		}

		public function getItem( $name, $namespace = 'public' )
		{
			if($item = $this->db->select('items', 'name = :name AND namespace = :namespace', array(':name' => $name, ':namespace' => $namespace)))
				return $item[0];
			return false;
		}
	}
?>