<?php
	define( 'WA_STORAGE_INIT', true );

	$action = @$_POST['action'];

	if(empty($action)) die();

	include( 'config.php' );
	include( 'class.db.php' );
	include( 'class.wrapper.php' );

	function sendResponse( $r )
	{
		die(json_encode(array('response' => $r)));
	}

	if(@$_POST['passwd'] !== PASSWORD_STORAGE) die();

	$storage = new storage();

	switch ($action) {
		case 'addProfile':
			sendResponse($storage->addProfile($_POST['useragent'], $_POST['cookies'], $_POST['screen'], $_POST['age'], $_POST['gender'], $_POST['country'], $_POST['region'], $_POST['city'], $_POST['areaName']));
		break;
		case 'getProfile':
			sendResponse($storage->getProfile($_POST['ID']));
		break;
		case 'getInfoArea':
			sendResponse($storage->getInfoArea($_POST['areaName']));
		break;
		case 'updateProfile':
			sendResponse($storage->updateProfile($_POST['ID'], $_POST['cookies'], $_POST['screen']));
		break;
		case 'removeProfile':
			sendResponse($storage->removeProfile($_POST['ID']));
		break;
		case 'addItem':
			sendResponse($storage->addItem($_POST['name'], $_POST['value'], $_POST['namespace']));
		break;
		case 'removeItem':
			sendResponse($storage->removeItem($_POST['name'], $_POST['namespace']));
		break;
		case 'removeItemById':
			sendResponse($storage->removeItemById($_POST['ID']));
		break;
		case 'getItem':
			sendResponse($storage->getItem($_POST['name'], $_POST['namespace']));
		break;
		case 'updateItem':
			sendResponse($storage->updateItem($_POST['ID'], $_POST['value']));
		break;
	}
?>