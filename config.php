<?php
	defined( 'WA_STORAGE_INIT' ) or die();

	define( 'DB_DSN', 'mysql:host=localhost;dbname=wiseantStorage' );
	define( 'DB_PASSWORD', 'YOUR_PASSWORD' );
	define( 'DB_USER', 'YOUR_USERNAME' );

	define( 'PASSWORD_STORAGE', 'YOUR_PASSWORD_FOR_STORAGE');
?>