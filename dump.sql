CREATE TABLE IF NOT EXISTS `items` (
  `ID` bigint(21) unsigned NOT NULL AUTO_INCREMENT,
  `namespace` varchar(100) NOT NULL DEFAULT 'public',
  `name` varchar(100) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `profiles` (
  `ID` bigint(21) unsigned NOT NULL AUTO_INCREMENT,
  `useragent` varchar(250) NOT NULL,
  `screen` varchar(100) NOT NULL,
  `cookies` text NOT NULL,
  `area` varchar(100) NOT NULL DEFAULT '_OTHER',
  `age` tinyint(3) NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `country` varchar(2) NOT NULL,
  `region` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;